// const Twit = require('twitter-v2');
const Twit = require('twit');
import { CSVService } from './csv.service';

// const twitterAPI = new Twit({
//     bearer_token: process.env.BEARER_TOKEN
// });

const twitterAPI = new Twit({
    consumer_key: process.env.API_KEY,
    consumer_secret: process.env.API_SECRET_KEY,
    access_token: process.env.ACCESS_TOKEN,
    access_token_secret: process.env.ACCESS_TOKEN_SECRET,
});

export class TwitterService {
    csvService = new CSVService();
    constructor() {
    }

    async search(query: TwitterQuery, count: number) {
        const q = this.buildQuery(query);
        count = count === null || count === undefined ? 10 : count;
        return new Promise((resolve, reject) => {
            twitterAPI.get('search/tweets', {q, count}, function(err: any, data: any, response: any){
                if (err) {
                    reject(err)
                } else {
                    resolve(data);
                }
            });
        });
    }

    buildQuery(q: TwitterQuery): string {
        const queryParams = [];
        if (q.hashTags.length > 0) 
            queryParams.push(q.hashTags.join(','));
        // if (q.since)
        //     queryParams.push(`since:${q.since.getFullYear()}-${q.since.getMonth()}-${q.since.getDate()}`);


        return queryParams.length > 0 ? queryParams.join(' ') : '';
    }

    async getUsers(usernames: string[]): Promise<any[]> {
        return new Promise(async (resolve, reject) => {
            if(!usernames || usernames.length < 1) {
                resolve([]);
            } else {
                const screen_names = [];
                while (usernames.length > 0)
                    screen_names.push(usernames.splice(0, 100));
                const userData = (await Promise.all(screen_names.map(async(screen_name) => {
                    return new Promise((resolve, reject) => {
                        twitterAPI.get(`/users/lookup`, {screen_name}, function(err: any, data: any, response: any){
                            if (err) {
                                reject(err)
                            } else {
                                resolve(data);
                            }
                        });
                    })
                }))) as any[];
                const data: any[] = [];
                userData.forEach((d: any) => {
                    d.forEach((user: any) => {
                        data.push(user);
                    });
                });
                resolve(data);
            }
        });
    }

    async parseUsers(input: string): Promise<any> {
        const data = (await this.csvService.getCsvData(input)) as any[];
        const usernameList = data.map((d:any) => {
            return d.username;
        });
        console.log("usernameList ", usernameList);
        const userData = await this.getUsers(usernameList);
        return userData;
    }
}

export class TwitterQuery {
    constructor() {}
    hashTags: string[] = [];
    since: Date = new Date();

    addHashTag(tag: string): void {
        if (tag && tag.length > 0){
            if (tag.substring(0,1) !== '#') {
                tag = `#${tag}`;
            }
            this.hashTags.push(tag);
        }
    }
}

// T.get('search/tweets', {q: '#tesla since:2021-03-01', count: 10}, function(err, data, response){
//     const tweets = data;
//     console.log(tweets);
// });