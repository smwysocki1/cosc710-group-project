import {createObjectCsvWriter} from 'csv-writer';
const csv = require('csv-parser');
const fs = require('fs');

export class CSVService {
    constructor() {}

    async write(data: any, file: string, mapper: any) {
        let headers: string[] = Object.keys(mapper);
        const csvWriter = createObjectCsvWriter({
            path: `output/${file}`,
            header: headers.map(header => {
                return {
                    id: header,
                    title: mapper[header]
                }
            })
        });
        return await csvWriter.writeRecords(data);

    }
    async writeUnique(initialData: any[], unique: string, file: string, mapper: any) {
        const data: any[] = [];
        initialData.forEach((d:any) => {
            if(!data.some(result => result[unique] === d[unique])) {
                data.push(d);
            }
        });
        console.log('usernames: ', data.length);

        let headers: string[] = Object.keys(mapper);
        const csvWriter = createObjectCsvWriter({
            path: `output/${file}`,
            header: headers.map(header => {
                return {
                    id: header,
                    title: mapper[header]
                }
            })
        });
        return await csvWriter.writeRecords(data);
    }

    async getCsvData(input: string) {
        return new Promise((resolve, reject) => {
            const data: any[] = [];
            fs.createReadStream(input)
            .pipe(csv())
            .on('data', (row: any) => {
                const key = Object.keys(row)[0];
                const attributes = key.split('\t');
                const values = row[key].split('\t');
                const result: any = {};
                attributes.forEach((a:string, index:number) => {
                    result[a] = values[index];
                });
                data.push(result);
            })
            .on('end', () => {
                console.log('CSV file successfully processed');
                resolve(data);
            });
        });
    }
}
