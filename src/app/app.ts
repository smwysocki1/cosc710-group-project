import { TwitterService, TwitterQuery } from "./service/twitter.service";
import { CSVService } from './service/csv.service';
import { TweetCSVMapper, UserCSVMapper } from './CSVMapper/csvMapper';
const inquirer = require('inquirer');

export class App {
    twitterService = new TwitterService();
    csvService = new CSVService();
    constructor() {
        
    }

    async init() {
        try {
            await this.askWhatToDo();
            console.log('Thanks for using the Custom Twitter Service. Come again');
        } catch(err) {
            if(err.isTtyError) {
                console.error('Prompt couldn\'t be rendered in the current environment');
              } else {
                console.error(err);
              }
            console.error('Session has ended');
        }
    }

    async askWhatToDo() {
        const answer = await inquirer.prompt({
            type: 'list',
            name: 'task',
            message: 'What would you like to do?',
            choices: ['Parse twint users', 'Search Twitter for Tweets', 'I am Done!'],
        });

        if (answer.task === 'Search Twitter for Tweets') {
            await this.searchTwitterForTweets(new TwitterQuery());
            await this.askWhatToDo();
        } else if (answer.task === 'Parse twint users') {
            await this.parseTwintUsers();
        }else {
            return;
        }
    }

    async parseTwintUsers() {
        const inputAnswer = await inquirer.prompt({
            type: 'input',
            name: 'path',
            message: 'What is the path to the file?',
            default: './input/tweets.csv',
        });
        const outputAnswer = await inquirer.prompt({
            type: 'input',
            name: 'path',
            message: 'What is the path to the output file?',
            default: 'users.csv',
        });
        const userData = await this.twitterService.parseUsers(inputAnswer.path);
        await this.csvService.write(userData, outputAnswer.path, new UserCSVMapper());
    }

    async searchTwitterForTweets(queryParams: TwitterQuery): Promise<any> {
        const answer = await inquirer.prompt([{
                type: 'list',
                name: 'search',
                message: 'What would you like to search by?',
                choices: ['Hash Tag', 'Date Sent']
            }
        ]);
        if (answer.search === 'Hash Tag') {
            queryParams.addHashTag(await this.askForHashTag());
        } else if (answer.search === 'Date Sent') {
            queryParams.since = await this.askForSentDate();
        }
        

        const addAnotherParameter = await inquirer.prompt([{
            type: 'confirm',
            name: 'askAgain',
            message: 'Would you like to add another parameter? (Hit enter for yes)',
            default: true,
        }]);
        if (addAnotherParameter.askAgain) {
            return await this.searchTwitterForTweets(queryParams);
        } else {
            const count = await this.askForHowManyTweets();
            const now = new Date();
            const filename = `search-${now.toUTCString()}.csv`;
            const tweets:any = await this.twitterService.search(queryParams, count);
            console.log('tweets: ', tweets);
            await this.csvService.write(tweets.statuses, filename, new TweetCSVMapper());
            return await this.updateUsersByTweets(tweets.statuses, now);
        }

    }

    async askForHashTag(): Promise<string> {
        const answer = await inquirer.prompt({
            type: 'input',
            name: 'hashTag',
            message: 'What hashTag would you like to search by?',
            default: '#',
            validate: function (value: string) {
            var pass = value.match(
                /^#\w+$/
            );
            if (pass) {
                return true;
            }
        
            return 'Please enter a valid twitter hash tag';
            }
        });
        return answer.hashTag;
    }

    async askForSentDate(): Promise<Date> {
        const now = new Date();
        const answer = await inquirer.prompt({
            type: 'input',
            name: 'sent',
            message: 'What date would you like to start the query from?',
            default: `${now.getFullYear()}-${now.getMonth()}-${now.getDate()}`,
            // validate: function (value) {
            // var pass = value > 0;
            // if (pass) {
            //     return true;
            // }
        
            // return 'Please provide a valid date.';
            // }
        });
        return new Date(answer.sent);
    }

    async askForHowManyTweets(): Promise<number> {
        const answer = await inquirer.prompt({
            type: 'number',
            name: 'count',
            message: 'How many tweets do you want back?',
            default: '10',
            validate: function (value: number) {
            var pass = value > 0 && value <= 100;
            if (pass) {
                return true;
            }
        
            return 'Please provide a number between 0 and 100.';
            }
        });
        return answer.count;
    }

    async updateUsersByTweets(tweets: any[], now: Date) {
        const users = await this.getUsers(tweets);
        const filename = `users-${now.toUTCString()}.csv`;
        await this.csvService.writeUnique(users, 'screen_name', filename, new UserCSVMapper());
    }

    async getUsers(tweets: any[]) {
        let users:string[] = [];
        let usernames:string[] = [];
        await Promise.all(tweets.map(async(tweet:any) => {
            users.push(tweet.user);
            const tweetUsernames = tweet.text.match(/@\w+/g);
            usernames.concat(tweetUsernames);
        }));
        return users.concat(await this.twitterService.getUsers(usernames));
    }
}